<?php
/* @var $this MessageController */
/* @var $model Message */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'message-form',
		'htmlOptions'=>array('class'=>'well'),
		)); ?>

		<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/avatar_images/'.$userId->image); ?>


		<?php
		echo $form->textFieldRow($userId, 'lastname', array('class'=>'span3'));
		 ?>
		<?php 
		echo $form->textAreaRow($model, 'text', array('class'=>'span8', 'rows'=>5));
		?>

		<div class="form-actions">
				<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Отправить', 'icon'=>'ok'));?>
		</div>

	<?php $this->endWidget(); ?>


</div>