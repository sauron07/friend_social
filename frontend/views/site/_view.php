<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">
	<hr />
	<b><?php echo CHtml::encode($data->getAttributeLabel('firstname')); ?>:</b>
	<?php echo CHtml::encode($data->firstname), ' ', CHtml::encode($data->lastname); ?>
	<br /><img src="<?php  echo  Yii::app()->request->baseUrl.'/images/avatar_images/'.$data->image ?>" alt="" width="50"><br />
	<?php echo CHtml::link('Профиль', array('profile/seeprofile', 'user_id'=>$data->user_id)); ?>	<br />
	<?php echo CHtml::link('Добавить в друзья', array('message/create', 'user_id'=>$data->user_id)); ?>	<br />
	<?php echo CHtml::link('Написать сообщение', array('message/create', 'user_id'=>$data->user_id)); ?>	<br />
	

</div>