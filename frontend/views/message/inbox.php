<?php
/* @var $this MessageController */
/* @var $dataProvider CActiveDataProvider */

// var_dump(Yii::app()->user->id);


$this->menu=array(
	array('label'=>'Create Message', 'url'=>array('create')),
	array('label'=>'Manage Message', 'url'=>array('admin')),
);
?>

<h1>Сообщения</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	// 'viewData'=>array('userId'=>$userId),
	'itemView'=>'_view',
)); ?>
