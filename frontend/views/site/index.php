<?php
$this->pageTitle=Yii::app()->name . ' - Home';
?>

<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'htmlOptions'=>array('class'=>'well'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	
	<p class="note">Для входа в систему введите логин и пароль.</p>
	<!-- <p class="note">Поля помеченные <span class="required">*</span> обязательны для заполнения</p> -->

	<?php echo $form->textFieldRow($model, 'username', array('class'=>'span3'));?>
	<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3'));?>




	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Вход', 'icon'=>'ok'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Сбросить', 'icon'=>'remove'));?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->