<?php

/**
 * This is the model class for table "tbl_user_profile".
 *
 * The followings are the available columns in table 'tbl_user_profile':
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $sex
 * @property string $datebirth
 * @property string $country
 * @property string $sity
 * @property string $street
 * @property string $image
 *
 * The followings are the available model relations:
 * @property TblUser $user
 */
class Profile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('image', 'unsafe'),
			array('firstname, lastname, sex, country, sity, street', 'length', 'max'=>255),
			array('datebirth', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, firstname, lastname, sex, datebirth, country, sity, street, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'TblUser', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'sex' => 'Пол',
			'datebirth' => 'Дата рождения',
			'country' => 'Страна',
			'sity' => 'Город',
			'street' => 'Улица',
			'image' => 'Аватар',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('datebirth',$this->datebirth,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('sity',$this->sity,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}   

	public function afterSave(){

		parent::afterSave();
	        	if (!$this->isNewRecord) {

	        		$model = new Profile;
            		$model->updateByPk($this->user_id, array('image' => $this->user_id.'_'.$this->image));
	        	}
	}
}