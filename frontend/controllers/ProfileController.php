<?php
	/**
	* Контроллер профиля
	*/
	class ProfileController extends Controller
	{
		
		public $layout='//layouts/column1';
		private $_model;


		public function actionEditprofile()

		{
			$model = $this -> loadmodel();

			if (isset($_POST['Profile'])) {
	            $model->attributes = $_POST['Profile'];

	            $file_image =  CUploadedFile::getInstance($model, 'image');

	            

        		if(is_object($file_image) && get_class($file_image) === 'CUploadedFile')
            		$model->image = $file_image;


	            	if ($model->save())
	            		{
	            			// Удаление не работает.
	            			if(file_exists($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/'. '/avatar_images'.'/'.$model->image))
									{
										//удаляем картинку и все её thumbs
										@unlink($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/'. '/avatar_images'.'/'.$model->image);
										@unlink($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'./images'. '/avatar_images'.'/thumbs/'.$model->image);
										@unlink($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'./images'. '/avatar_images'.'/thumbs_small/'.$model->image);						
										$model->image = '';
									}

				            if(is_object($file_image)) 
					            {
					            	// сохранение файла на сервер под именем id+image_name
					            	
					            	$dir=$_SERVER['DOCUMENT_ROOT'].Yii::app()->request->baseUrl.'/images/' . '/avatar_images';
					            	$file=$dir.'/'.$model->user_id.'_'.$model->image;
					            	$model->image->saveAs($file);
					            	//Используем функции расширения CImageHandler;
									$ih = new CImageHandler(); //Инициализация
									Yii::app()->ih 
									->load($file) //Загрузка оригинала картинки
									->thumb('200', false) //Создание превьюшки шириной 200px
									->save($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/'. '/avatar_images'.'/thumbs/'.$model->user_id.'_'.$model->image) //Сохранение превьюшки в папку thumbs
									->reload() //Снова загрузка оригинала картинки
									->thumb('50', '50') //Создание превьюшки размером 50px
									->save($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/'. '/avatar_images'.'/thumbs_small/'.$model->user_id.'_'.$model->image) //Сохранение превьюшки в папку thumbs_small
									->reload()//Снова загрузка оригинала картинки
									->thumb('800', '800') //Создание превьюшки размером 800px
									->save($_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/'. '/avatar_images'.'/'.$model->user_id.'_'.$model->image) //Сохранение большой картинки в папку
									;
					            }
					        
				            $this->redirect(array('profile', 'id' => $model->user_id));
				        }

	        	        
		    }

			$this->render('editprofile', array(
				'model' => $this -> loadmodel()
			));

		}


		public function actionProfile()

		{
			$this->render('profile', array(
				'model' => $this -> loadmodel()
			));

		}

		public function actionSeeProfile($user_id)
		
		{
			$userProfile = Profile::model()->find('user_id = :user_id', array(':user_id' => $user_id));

			$model=new Message;

			$dataProvider = new CActiveDataProvider('Profile');

			$this->render('seeprofile',array(
				'model'=>$model,
				'userProfile'=>$userProfile,
				'dataProvider' => $dataProvider,
			));
		}

		public function loadModel()
		{
			if($this->_model===null)
			{
				if(isset($_GET['id']))
					$this->_model=Profile::model()->findbyPk($_GET['id']);
				if($this->_model===null)
					throw new CHttpException(404,'The requested page does not exist.');
			}
			return $this->_model;
		}
	}
?>