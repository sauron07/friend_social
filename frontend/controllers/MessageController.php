<?php

class MessageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create'),
				'users'=>array('@'),
			),
		);
	}


	public function actionCreate($user_id)
	{
		// прописал все заново, и заработало
		$userId = Profile::model()->find('user_id = :user_id', array(':user_id' => $user_id));

		$model=new Message;

		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
			$model->id_to = $userId->user_id;
			if($model->save())
				$this->redirect(array('outbox'));

		}

		$this->render('create',array(
			'model'=>$model,
			'userId'=>$userId,
		));
	}


	public function loadModel($user_id)
	{
		$model=Profile::model()->findByPk($user_id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		// $model = new Profile;

		$criteria = new CDbCriteria;
		$curentUser = Yii::app()->user->id;
		$criteria -> condition = "id_to = $curentUser" ||  "id_from = $curentUser";

        $dataProvider = new CActiveDataProvider('Message', array(
        		'criteria' => $criteria,
        	));

        $this->render ('index', array(
        	'dataProvider'=>$dataProvider,
        	// 'model' => $model,
        	));


		// $userId = Profile::model()->find('user_id = :user_id', array(':user_id' => $user_id));

		// $model = new Profile;

		// $criteria=new CDbCriteria;
		// $curentUser = Yii::app()->user->id;
		// $criteria -> condition = "id_from = $curentUser";
		

		// $dataProvider=new CActiveDataProvider('Message', array(
		// 	'criteria' => $criteria,));
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// 	'model' => $model,
		// 	// 'userId' => $userId,
		// ));
	}
	// Входящие сообщения
	public function actionInbox(){


		$criteria=new CDbCriteria;
		$curentUser = Yii::app()->user->id;
		$criteria -> condition = "id_to = $curentUser";
		

		$dataProvider=new CActiveDataProvider('Message', array(
			'criteria' => $criteria,));
		$this->render('inbox',array(
			'dataProvider'=>$dataProvider,
		));
	}
	// Исходящие сообщения
	public function actionOutbox(){
		$criteria=new CDbCriteria;
		$curentUser = Yii::app()->user->id;
		$criteria -> condition = "id_from = $curentUser";
		

		$dataProvider=new CActiveDataProvider('Message', array(
			'criteria' => $criteria,));
		$this->render('outbox',array(
			'dataProvider'=>$dataProvider,
		));

	}


}
