<?php
/**
 * main.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:31 AM
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css">
	
	<!-- Use the .htaccess and remove these lines to avoid edge case issues. More info: h5bp.com/b/378 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php echo h($this->pageTitle); /* using shortcut for CHtml::encode */ ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">


	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css"/>
	<!--using less instead? file not included-->
	<!--<link rel="stylesheet/less" type="text/css" href="/less/styles.less">-->

	<!-- create your own: http://modernizr.com/download/-->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr-2.6.2.js"></script>

	<!--<script src="/less/less-1.3.0.min.js"></script>-->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico">
</head>

<body>
<div class="container" id="page">
	<?php
	 $this->widget('bootstrap.widgets.TbNavbar', array(
	'type' => 'inverse', // null or 'inverse'
	'brand' => 'Friend social',
	'brandUrl' => "Yii::app()->request->baseUrl/site/index",
	'collapse' => true, // requires bootstrap-responsive.css
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => array(
				array('label' => 'Регистрация', 'url' => array('/user/registration'), 'visible'=> Yii::app()->user->isGuest), 
				array(
			            		'label' => 'Профиль', 
			            		'url' => array('/profile/profile', 'id'=>Yii::app()->user->id), 
			            		'visible' => !Yii::app()->user->isGuest
            				),
				array(
			            		'label' => 'Люди', 
			            		'url' => array('/site/people'), 
			            		'visible' => !Yii::app()->user->isGuest
            				),
				array(
			            		'label' => 'Сообщения', 
			            		// 'url' => array('/message/index'), 
			            		'visible' => !Yii::app()->user->isGuest,
			            		'url' => array('message/index'),
			            		// 'items' => array(
			            		// 		array(
						           //  		'label' => 'Входящие', 
						           //  		'url' => array('/message/inbox'), 
						           //  		'visible' => !Yii::app()->user->isGuest
            					// 		),
			            		// 		array(
						           //  		'label' => 'Исходящие', 
						           //  		'url' => array('/message/outbox'), 
						           //  		'visible' => !Yii::app()->user->isGuest
            					// 		),
			            		// 	),
            				),
			
			),
		),
		
		//(!Yii::app()->user->isGuest) ? '<p class="navbar-text pull-right">Вошли как <a href="#">' . Yii::app()->user->name . '</a></p>' : '',
		

		array(
            'class'=>'bootstrap.widgets.TbMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
            	array(
			            		'label' => 'Login', 
			            		'url' => array('/site/login'), 
			            		'visible' => Yii::app()->user->isGuest
			            	),
            	array(
            		'label'=>Yii::app()->user->name,
            		'visible'=>!Yii::app()->user->isGuest,
            		'items'=>array(
            				array(
			            		'label' => 'Профиль', 
			            		'url' => array('/profile/profile', 'id'=>Yii::app()->user->id), 
			            		'visible' => !Yii::app()->user->isGuest
            				),
			            	array(
			            		'label'=>'Редактировать профиль', 
			            		'url'=>array('/profile/editprofile', 'id'=>Yii::app()->user->id), 
			            		'visible' => !Yii::app()->user->isGuest
			            	),
			            	
							array('label' => 'Logout (' . Yii::app()->user->name . ')', 
								'url' => array('/site/logout')
							), 
            		),
            	)
            ),
        ),



	),
)); 
?>
	<!-- mainmenu -->
	<div class="container" style="margin-top:80px">
		<?php if (isset($this->breadcrumbs)): ?>
			<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links' => $this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
		<?php endif?>

		<?php echo $content; ?>
		<hr/>
		<div id="footer">
			<!-- Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/> -->
			<!-- All Rights Reserved.<br/> -->
			<?php
			 // echo Yii::powered(); 
			 ?>
		</div>
		<!-- footer -->
	</div>
</div>




<!-- Google Analytics -->
<script>
	var _gaq=[['_setAccount','<?php echo param('google.analytics.account'); // check global.php shortcut file at "common/lib/" ?>'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
</body>
</html>