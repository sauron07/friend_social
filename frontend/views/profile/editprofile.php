<?php
	echo "Редактировать профиль ", Yii::app()->user->name ;
?>

<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'editprofile',
	//'enableClientValidation'=>true,
	'htmlOptions'=>array(
		'class'=>'well',
		'enctype'=>'multipart/form-data',
		),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<!-- <p class="note">Поля обозначены <span class="required">*</span> обязательны для заполнения.</p> -->
	<?php

	// var_dump($this)
	?>
	<?php 
	// echo $form->textFieldRow($model, 'id', array('class'=>'span3'));
	?>
	<?php echo $form->textFieldRow($model, 'firstname', array('class'=>'span3'));?>
	<?php echo $form->textFieldRow($model, 'lastname', array('class'=>'span3'));?>
	<?php echo $form->dropDownListRow($model, 'sex', array('Выберите Ваш пол', 'Мужчина', 'Женщина'), array('class'=>'span2'));?></br>

	<h7>дата рождения</h7></br>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model'=>$model, 'attribute'=>'datebirth',
    'options'=>array(
        'dateFormat'=>'yy-mm-dd',
        'yearRange'=>'-70:+0',
        'changeYear'=>'true',
        'changeMonth'=>'true',
        'label'=>'Дата рождения',
 		'defaultDate'=>'+0'
    ),
)); ?>





	<?php echo $form->textFieldRow($model, 'country', array('class'=>'span3'), array(
                    
                    ));?>
	<?php echo $form->textFieldRow($model, 'sity', array('class'=>'span3'));?>
	<?php echo $form->textFieldRow($model, 'street', array('class'=>'span3'));?>
	<?php echo $form->fileFieldRow($model, 'image', array('class'=>'span3'));?>
	
	<?php 
	   	 // echo CHtml::image($model->image);
	?>

	<?php
	 // echo $form->passwordFieldRow($model, 'password', array('class'=>'span3'));
	 ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Submit', 'icon'=>'ok'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Reset'));?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
