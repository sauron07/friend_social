<?php 
	Class UserController extends Controller{

		public $layout='//layouts/column1';

		public function actions(){
			return array(
					'captcha' => array(
							'class' => 'CCaptchaAction',
							'maxLength' => 3,
							'minLength' => 3,
						),
				);
		}

		public function accessRules(){

			return array(
					array('allow',
						'actions' => array('registration'),
						'users' => '*'),
				);
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}

		// Метод входа на сайт
		public function actionusername(){

		}

		// Метод выхода с сайта
		public function actionLogout(){

		}


		public function actionRegistration(){
        	// тут думаю все понятно
        	$model = new User();
	        // $form = new User();
	        

	        // Проверяем являеться ли пользователь гостем
	        // ведь если он уже зарегистрирован - формы он не должен увидеть.
	        if (!Yii::app()->user->isGuest) {
	             throw new CException('Вы уже зарегистрированны!');
	        } else {
	            // Если $_POST['User'] не пустой массив - значит была отправлена форма
	            // следовательно нам надо заполнить $form этими данными
	            // и провести валидацию. Если валидация пройдет успешно - пользователь
	            // будет зарегистрирован, не успешно - покажем ошибку на экран
	            if (!empty($_POST['User'])) {
	                
	                 // Заполняем $form данными которые пришли с формы
	                $model->attributes = $_POST['User'];
	                
	                // Запоминаем данные которые пользователь ввёл в капче
	                 $model->verifyCode = $_POST['User']['verifyCode'];
	                
	                    // В validate мы передаем название сценария. Оно нам может понадобиться
	                    // когда будем заниматься созданием правил валидации [читайте дальше]
	                     if($model->validate('registration')) {
	                        // Если валидация прошла успешно...
	                        // Тогда проверяем свободен ли указанный логин..

	                            if ($model->model()->count("username = :username", array(':username' => $model->username))) {
	                                 // Указанный логин уже занят. Создаем ошибку и передаем в форму
	                                $model->addError('username', 'Логин уже занят');
	                                $this->render("registration", array('model' => $model));
	                             } else {
	                                // Выводим страницу что "все окей"
	                                $model->save();
	                                $this->render("registration_ok", array('model' => $model));
	                            }
	                                             
	                    } else {
	                        // Если введенные данные противоречат 
	                        // правилам валидации (указаны в rules) тогда
	                        // выводим форму и ошибки.
	                         // [Внимание!] Нам ненадо передавать ошибку в отображение,
	                        // Она автоматически после валидации цепляеться за 
	                        // $form и будет [автоматически] показана на странице с 
	                         // формой! Так что мы тут делаем простой рэндер.
	                        
	                        $this->render("registration", array('model' => $model));
	                    }
	             } else {
	                // Если $_POST['User'] пустой массив - значит форму некто не отправлял.
	                // Это значит что пользователь просто вошел на страницу регистрации
	                // и ему мы должны просто показать форму.
	                 
	                $this->render("registration", array('model' => $model));
	            }
	        }
    	}

    	public function actionRegistrationOk(){

    		$model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(array('profile/profile', 'id'=>Yii::app()->user->id));
        }
        // display the login form
        $this->render('redistrationok', array('model' => $model));
    	}


	}
?>